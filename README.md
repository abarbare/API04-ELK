Contribution API04
==================

Ressources disponibles:
-----------------------
- Images Docker des produits pour reproduire la stack ELK
    - Fichiers de configurations 
    - Docker-compose pour le lancement
- Module de présentation Scenari 
- Fichiers de configuration logstash pour traiter les données et les charger les données dans une base elasticsearch
- Fichiers bruts issus de l'Open Data de la SNCF