require "logstash/filters/base"
require "logstash/namespace"

class LogStash::Filters::Localisation < LogStash::Filters::Base


  # filter {
  #   localisation { ... }
  # }
  config_name "localisation"

  # Ficher csv avec les ville et les latitudes,longitudes correspondantes 
  config :database, :validate => :path
  config :source, :validate => :string, :required => true
  config :target, :validate => :string, :default => 'location'
  config :locs 

  public
  def register
    if @database.nil?
      @database = LogStash::Environment.vendor_path("/root/localisation.csv")
      if !File.exists?(@database)
        raise "la ligne  'database => ...' est obligatoire"
      end
    end
    
    @locs = Hash.new
    CSV.foreach(@database) do |row|
      # Lecture des infos du fichier csv des villes
      @locs[row[0]] = [ row[2].to_f, row[1].to_f ] # { "lat" => row[1].to_f, "lon" => row[2].to_f }
    end
  end
  

  public
  def filter(event)
    # Ne retourne rien si la fonction ne renvoie rien 
    return unless filter?(event)

    if event[@source]
      loc = event[@source]
      if @locs[loc]
        event[@target] = @locs[loc]
        else
        @logger.error("Localisation inconnue: ", {"loc" => loc})
      end
    end
    # Les données récupérées sont renvoyées 
    filter_matched(event)
  end
end # class LogStash::Filters::Foo

